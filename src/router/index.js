import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home';
import MyLists from '../components/MyLists';
import NewList from '../components/NewList';
import MyList from '../components/MyList';

Vue.use(Router);

export default new Router({
  routes: [
      {
          path: '/',
          name: 'Home',
          component: Home
      },
      {
          path: '/listing',
          name: 'MyLists',
          component: MyLists,
          props: true
      },
      {
          path: '/new',
          name: 'NewList',
          component: NewList
      },
      {
          path: '/list/:id',
          name: 'MyList',
          component: MyList
      },
  ]
})
